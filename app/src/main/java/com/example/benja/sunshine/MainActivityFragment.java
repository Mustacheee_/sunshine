package com.example.benja.sunshine;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.benja.sunshine.com.jbink.sunshine.database.WeatherContract;
import com.example.benja.sunshine.com.jbink.sunshine.database.WeatherDBHelper;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    Button search;
    TextView infoDisplay;
    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        this.infoDisplay = (TextView) rootView.findViewById(R.id.main_display_info);
        displayDefaultInfo();

        this.search = (Button) rootView.findViewById(R.id.mainSearchBtn);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), SearchActivity.class);
                startActivity(i);
            }
        });
       // getContext().deleteDatabase(WeatherContract.LocationEntry.TABLE_NAME);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        displayDefaultInfo();

    }

    private void displayDefaultInfo(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        String location = preferences.getString(getString(R.string.pref_location_key), getString(R.string.pref_location_default)).trim();

        SQLiteDatabase db = new WeatherDBHelper(getContext()).getWritableDatabase();
        String whereClause = WeatherContract.LocationEntry.COLUMN_CITY_NAME + " = ?";
        String[] whereArgs = new String[]{location};

        Cursor c = db.query(WeatherContract.LocationEntry.TABLE_NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null);

        if(c.moveToFirst()){
            int locationId = c.getInt(c.getColumnIndex(WeatherContract.LocationEntry._ID));

            whereClause = WeatherContract.WeatherEntry.COLUMN_LOC_KEY + " = ?";
            whereArgs = new String[]{locationId+""};

            Cursor wc = db.query(WeatherContract.WeatherEntry.TABLE_NAME,
                    null,
                    whereClause,
                    whereArgs,
                    null,
                    null,
                    null);

            if(wc.moveToFirst()){
                infoDisplay.setText("");
                infoDisplay.append(location+'\n');

                Double currentTemp = wc.getDouble(wc.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_TEMP));
                infoDisplay.append(formatTemp(currentTemp)+'\n');

                infoDisplay.append(wc.getString(wc.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_SHORT_DESC)) + '\n');

                Double max_temp = wc.getDouble(wc.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_MAX_TEMP));
                Double min_temp = wc.getDouble(wc.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_MIN_TEMP));
                infoDisplay.append(formatTemp(max_temp) + '\\' + formatTemp(min_temp) + '\n');

                infoDisplay.append("Humidity " +
                        wc.getDouble(wc.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_HUMIDITY)) +
                        "%\n");
                infoDisplay.append("Wind " + formatWind(wc.getInt(wc.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_WIND_SPEED)),
                        wc.getInt(wc.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_DEGREES))));
                wc.close();
            }else{
                Log.v("Main Activity", "Couldnt retrieve weather info from location");
            }
        }else{
            Log.v("Main Activity", "Couldnt find location");
        }
        c.close();
        db.close();

    }

    private String formatTemp(Double temp){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String units = preferences.getString(getString(R.string.pref_units_key), getString(R.string.pref_units_default));
        switch (units){
            case "imperial":
                return Math.round(((temp - 273.15) * 1.8) + 32)+"F";
            case "metric":
                return Math.round(temp - 273.15)+"C";
            case "kelvin":
                return temp+"K";
            default:
                break;
        }
        return "Error computing temperature";
    }

    private String formatWind(int windSpeed, int windDegrees){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String units = preferences.getString(getString(R.string.pref_units_key), getString(R.string.pref_units_default));
        String newWind = windSpeed+"m/s";
        String direction;

        if(units.equals("imperial"))
            newWind = Math.round(windSpeed * 2.24)+"MPH";

        int result = ((int)Math.round(windDegrees / 11.25))%16;
        switch (result){
            case (0):
                direction = "N";
                break;
            case(1):
                direction = "NNE";
                break;
            case(2):
                direction = "NE";
                break;
            case(3):
                direction = "ENE";
                break;
            case(4):
                direction = "E";
                break;
            case(5):
                direction = "ESE";
                break;
            case (6):
                direction = "SE";
                break;
            case(7):
                direction = "SSE";
                break;
            case(8):
                direction = "S";
                break;
            case(9):
                direction = "SSW";
                break;
            case(10):
                direction = "SW";
                break;
            case(11):
                direction = "WSW";
                break;
            case (12):
                direction = "W";
                break;
            case(13):
                direction = "WNW";
                break;
            case(14):
                direction = "NW";
                break;
            case(15):
                direction = "NNW";
                break;
            default:
                direction = "Unknown Direction";
                break;

        }
        return direction + " " + newWind;
    }
}
