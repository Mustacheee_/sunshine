package com.example.benja.sunshine;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.format.Time;
import android.util.Log;
import android.widget.EditText;

import com.example.benja.sunshine.com.jbink.sunshine.database.WeatherContract;
import com.example.benja.sunshine.com.jbink.sunshine.database.WeatherDBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Vector;

/**
 * Created by benja on 3/1/2016.
 */
public class FetchWeatherTask extends AsyncTask<Void, Void, String[]> {
    String name;
    Context context;

    public FetchWeatherTask(String name, Context context){
        this.name = name;
        this.context = context;
    }
    @Override
    protected String[] doInBackground(Void... params) {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority("api.openweathermap.org")
                .appendPath("data")
                .appendPath("2.5")
                .appendPath("weather")
                .appendQueryParameter("q", name)
                .appendQueryParameter("APPID", MainActivity.APIKEY);

        InputStream in = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(builder.build().toString());
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            in = new BufferedInputStream(urlConnection.getInputStream());
            StringBuffer buffer = new StringBuffer();


            if(in == null)
                return null;

            reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null){
                buffer.append(line + '\n');
            }

            urlConnection.disconnect();
            String[] results = getWeatherDataFromJson(buffer.toString());

            return results;

        } catch (MalformedURLException e) {
            Log.e("FetchWEatherTask!!!", e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e("FUUUUCK", e.toString());
        } finally {
            if(in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e("FetchWeatherTask!!!", "Error closing InputStream");
                }
            if(reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e("FetchWeatherTask!!!", "Error closing BufferedReader");
                }

        }


        return null;
    }

    private String[] getWeatherDataFromJson(String jsonStr) throws JSONException{
        //Coordinates information
        final String COORD_PARAM = "coord";
        final String LAT_PARAM = "lat";
        final String LONG_PARAM = "lon";

        //Weather Information
        final String WEATHER_PARAM = "weather";
        final String WEATHER_ID = "id";
        final String WEATHER_MAIN = "main";
        final String WEATHER_DESC = "description";

        //Date Information
        final String DATE_PARAM = "dt";


        //Temperature Information
        final String TEMP_PARAM = "main";
        final String TEMP = "temp";
        final String TEMP_MAX = "temp_max";
        final String TEMP_MIN = "temp_min";
        final String TEMP_PRESSURE = "pressure";
        final String TEMP_HUMIDITY = "humidity";

        //Wind Information
        final String WIND_PARAM = "wind";
        final String WIND_SPEED = "speed";
        final String WIND_DEG = "deg";

        //City Information
        final String CITY_ID = "id";
        final String CITY_NAME = "name";
        final String ERROR_CODE = "cod";

        //Initiate various JSON Objects
        JSONObject jsonObject = new JSONObject(jsonStr);

        //Check if query was successful
        int errorCode = jsonObject.getInt(ERROR_CODE);
        if(errorCode != 200){
            return null;
        }

        JSONObject jsonCoord = jsonObject.getJSONObject(COORD_PARAM);

        JSONArray jsonWeatherArray = jsonObject.getJSONArray(WEATHER_PARAM);
        JSONObject jsonWeather = jsonWeatherArray.getJSONObject(0);
        JSONObject jsonTemp = jsonObject.getJSONObject(TEMP_PARAM);
        JSONObject jsonWind = jsonObject.getJSONObject(WIND_PARAM);

        //Get City Information
        String name = jsonObject.getString(CITY_NAME);
        int cityID = jsonObject.getInt(CITY_ID);

        addLocation(name, jsonCoord.getDouble(LAT_PARAM), jsonCoord.getDouble(LONG_PARAM));
        addWeatherData(name, jsonObject.getLong(DATE_PARAM), jsonWeather.getString(WEATHER_MAIN),
                jsonWeather.getInt(WEATHER_ID),
                jsonTemp.getDouble(TEMP),
                jsonTemp.getDouble(TEMP_MIN),
                jsonTemp.getDouble(TEMP_MAX),
                jsonTemp.getInt(TEMP_HUMIDITY),
                jsonTemp.getDouble(TEMP_PRESSURE),
                jsonWind.getInt(WIND_SPEED),
                jsonWind.getInt(WIND_DEG));

        Vector<String> v = new Vector<String>();
        v.add("Name: " + name);
        v.add("City ID: " + cityID);
        v.add("Short_Description: " + jsonWeather.getString(WEATHER_MAIN));
        v.add("Min: " + jsonTemp.getDouble(TEMP_MIN));
        v.add("Max: " + jsonTemp.getDouble(TEMP_MAX));
        v.add("Humidity: " + jsonTemp.getInt(TEMP_HUMIDITY));
        v.add("Pressure: " + jsonTemp.getDouble(TEMP_PRESSURE));
        v.add("Error Code: " + errorCode);
        v.add("Wind Speed: " + jsonWind.getInt(WIND_SPEED));
        v.add("Wind Degrees: " + jsonWind.getInt(WIND_DEG));

        String[] results = new String[v.size()];
        int i = 0;
        for(String s : v){
            results[i] = s + '\n';
            i++;
        }
        return results;
    }

    private long addLocation(String name, double lat, double lon){
        SQLiteDatabase db = new WeatherDBHelper(context).getWritableDatabase();

        String whereClause = WeatherContract.LocationEntry.COLUMN_CITY_NAME + " = ?";
        String[] whereArgs = new String[]{name};

        Cursor c = db.query(WeatherContract.LocationEntry.TABLE_NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null);

        if(c.moveToFirst())
            return -1;

        c.close();

        ContentValues values = new ContentValues();
        values.put(WeatherContract.LocationEntry.COLUMN_CITY_NAME, name);
        values.put(WeatherContract.LocationEntry.COLUMN_COORD_LAT, lat);
        values.put(WeatherContract.LocationEntry.COLUMN_COORD_LONG, lon);

        long rowId = db.insert(WeatherContract.LocationEntry.TABLE_NAME, null, values);
        db.close();
        return rowId;
    }

    private long addWeatherData(String name, long date, String short_desc, int weatherID, double currentTemp, double min, double max, double humidity, double pressure, int windSpeed, int degrees){
        final String CITY_NAME = "name";

        SQLiteDatabase db = new WeatherDBHelper(context).getWritableDatabase();

        //Get Location ID
        String where = WeatherContract.LocationEntry.COLUMN_CITY_NAME + " = ?";
        String[] whereA = new String[]{name};
        Cursor t = db.query(WeatherContract.LocationEntry.TABLE_NAME, null,
                where,
                whereA,
                null,
                null,
                null);
        t.moveToFirst();
        int locationID = t.getInt(t.getColumnIndex(WeatherContract.LocationEntry._ID));
        t.close();
        //Finish with Location ID

        String whereClause = WeatherContract.WeatherEntry.COLUMN_LOC_KEY + " = ? AND " +
                WeatherContract.WeatherEntry.COLUMN_DATE + " = ? ";

        String[] whereArgs = new String[]{""+locationID, ""+date};

        Cursor c = db.query(WeatherContract.WeatherEntry.TABLE_NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null);

        if(c.moveToFirst())
            return -1;

        c.close();

        ContentValues values = new ContentValues();
        values.put(WeatherContract.WeatherEntry.COLUMN_LOC_KEY, locationID);
        values.put(WeatherContract.WeatherEntry.COLUMN_DATE, date);
        values.put(WeatherContract.WeatherEntry.COLUMN_SHORT_DESC, short_desc);
        values.put(WeatherContract.WeatherEntry.COLUMN_WEATHER_ID, weatherID);
        values.put(WeatherContract.WeatherEntry.COLUMN_TEMP, currentTemp);
        values.put(WeatherContract.WeatherEntry.COLUMN_MIN_TEMP, min);
        values.put(WeatherContract.WeatherEntry.COLUMN_MAX_TEMP, max);
        values.put(WeatherContract.WeatherEntry.COLUMN_HUMIDITY, humidity);
        values.put(WeatherContract.WeatherEntry.COLUMN_PRESSURE, pressure);
        values.put(WeatherContract.WeatherEntry.COLUMN_WIND_SPEED, windSpeed);
        values.put(WeatherContract.WeatherEntry.COLUMN_DEGREES, degrees);

        long rowId = db.insert(WeatherContract.WeatherEntry.TABLE_NAME, null, values);

        Log.i("WEather DB", db.toString());
        db.close();

        return rowId;
    }
}
