package com.example.benja.sunshine;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.benja.sunshine.com.jbink.sunshine.database.WeatherContract;
import com.example.benja.sunshine.com.jbink.sunshine.database.WeatherDBHelper;

import java.text.SimpleDateFormat;
import java.util.concurrent.ExecutionException;

/**
 * A placeholder fragment containing a simple view.
 */
public class SearchActivityFragment extends Fragment {
    EditText cityName;
    Button search;
    TextView response;

    public SearchActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        this.cityName = (EditText) rootView.findViewById(R.id.searchCityName);
        this.search = (Button) rootView.findViewById(R.id.search_SearchBtn);
        this.response = (TextView) rootView.findViewById(R.id.search_responseTextView);

        SharedPreferences  preferences= PreferenceManager.getDefaultSharedPreferences(getContext());
        cityName.setText(preferences.getString(getString(R.string.pref_location_key), getString(R.string.pref_location_default)));

        this.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = cityName.getText().toString();
                if (name.equals(""))
                    name = "London";

                FetchWeatherTask fwt = new FetchWeatherTask(name, getContext());
                fwt.execute();

                try {
                    if (fwt.get() == null) {
                        Toast.makeText(getContext(), "Error retrieving data", Toast.LENGTH_SHORT).show();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                SQLiteDatabase db = new WeatherDBHelper(getContext()).getReadableDatabase();
                Cursor j = db.query(WeatherContract.WeatherEntry.TABLE_NAME, null,
                        null,
                        null,
                        null,
                        null,
                        null);

                Cursor t = db.query(WeatherContract.LocationEntry.TABLE_NAME,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);

                response.setText("");
                if (t.moveToFirst()) {
                    int rowCount = t.getCount();
                    response.append(WeatherContract.LocationEntry._ID + '\t' +
                            WeatherContract.LocationEntry.COLUMN_CITY_NAME + '\t' +
                            WeatherContract.LocationEntry.COLUMN_COORD_LAT + '\t' +
                            WeatherContract.LocationEntry.COLUMN_COORD_LONG + '\n');

                    for (int i = 0; i < rowCount; i++) {
                        response.append("" + t.getInt(t.getColumnIndex(WeatherContract.LocationEntry._ID)) + '\t');
                        response.append(t.getString(t.getColumnIndex(WeatherContract.LocationEntry.COLUMN_CITY_NAME)) + '\t');
                        response.append("" + t.getDouble(t.getColumnIndex(WeatherContract.LocationEntry.COLUMN_COORD_LAT)) + '\t');
                        response.append("" + t.getDouble(t.getColumnIndex(WeatherContract.LocationEntry.COLUMN_COORD_LONG)) + '\n');
                        if (!t.moveToNext())
                            break;
                    }
                    t.close();

                    if (j.moveToFirst()) {
                        int rCount = j.getCount();
                        response.append(WeatherContract.WeatherEntry.COLUMN_MAX_TEMP + '\t' + WeatherContract.WeatherEntry.COLUMN_MIN_TEMP + '\n');
                        for (int k = 0; k < rCount; k++) {
                            response.append("" + j.getDouble(j.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_MAX_TEMP)) + '\t');
                            response.append("" + j.getDouble(j.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_MIN_TEMP)) + '\t');
                            SimpleDateFormat df = new SimpleDateFormat("EEE MM dd");
                            String date = df.format(j.getLong(j.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_DATE)) * 1000L);
                            response.append(date + '\n');
                            if (!j.moveToNext())
                                break;
                        }
                    }
                }

                db.close();
            }
        });

        return rootView;
    }
}
